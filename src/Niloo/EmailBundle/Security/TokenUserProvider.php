<?php
/**
 * Created by IntelliJ IDEA.
 * User: ken
 * Date: 12/02/2017
 * Time: 20:57
 */

namespace Niloo\EmailBundle\Security;


use Doctrine\ORM\EntityRepository;
use Niloo\EmailBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class TokenUserProvider
 * @package Niloo\EmailBundle\Security
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class TokenUserProvider implements UserProviderInterface
{
    /**
     * @var EntityRepository
     */
    protected $userRepository;
    /**
     * @var EntityRepository
     */
    protected $tokenRepository;


    public function __construct(EntityRepository $tokenRepository, EntityRepository $userRepository)
    {
        $this->tokenRepository = $tokenRepository;
        $this->userRepository = $userRepository;
    }

    public function getToken($authTokenHeader)
    {
        return $this->tokenRepository->findOneByValue($authTokenHeader);
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $email The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($email)
    {
        return $this->userRepository->findOneByEmail($email);
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        // System is stateless, so it'll never refresh a user
        throw new UnsupportedUserException();
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
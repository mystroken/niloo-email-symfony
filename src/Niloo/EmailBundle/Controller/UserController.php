<?php
/**
 * Created by IntelliJ IDEA.
 * User: ken
 * Date: 12/02/2017
 * Time: 16:03
 */

namespace Niloo\EmailBundle\Controller;


use Niloo\EmailBundle\Entity\User;
use Niloo\EmailBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Form;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

/**
 * Class UserController
 *
 * @package Niloo\EmailBundle\Controller
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class UserController extends Controller
{


    /**
     * Get all users
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"user"})
     * @Rest\Get("/users")
     * @param Request $request
     * @return array|User[]
     */
    public function getUsersAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findBy([], ['id'=>'DESC']);
        return $users;
    }

    /**
     * Get detail of a specific user
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"user"})
     * @Rest\Get("/users/{id}")
     * @param Request $request
     * @return User|null|object
     */
    public function getUserAction(Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find((int) $request->get('id'));

        if(!$user) {
            return $this->_userNotFound();
        }

        return $user;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"user"})
     * @Rest\Post("/users")
     * @param Request $request
     * @return User|Form
     */
    public function postUsersAction(Request $request)
    {

        $user = new User();
        $form = $this->createForm(UserType::class, $user, ['validation_groups' => ['Default', 'New']]);

        $form->submit($request->request->all());

        if($form->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $user;
        }

        return $form;
    }

    /**
     * Update (put=completely) a user entity
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"user"})
     * @Rest\Put("/users/{id}")
     * @param Request $request
     * @return User|null|object|Form
     */
    public function updateUserAction(Request $request)
    {
        return $this->_updateUser($request, true);
    }

    /**
     * Update (patch=partially) a user entity
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"user"})
     * @Rest\Patch("/users/{id}")
     * @param Request $request
     * @return User|null|object|Form
     */
    public function patchUserAction(Request $request)
    {
        return $this->_updateUser($request, false);
    }


    /**
     * Update partially (clearMissing=false) or completely(clearMissing=true) a user entity
     *
     * @param Request $request
     * @param $clearMissing
     * @return User|null|object|Form
     */
    protected function _updateUser(Request $request, $clearMissing)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository(User::class)->find((int) $request->get('id'));

        if(empty($user)) {
            return $this->_userNotFound();
        }

        if ($clearMissing) { // If it is a complete update, the password must be validated
            $options = ['validation_groups'=>['Default', 'FullUpdate']];
        } else {
            $options = []; // We keep the Default validation group else
        }

        $form = $this->createForm(UserType::class, $user, $options);
        $form->submit($request->request->all(), $clearMissing);

        if($form->isValid()) {
            // If the user wants to change his password
            if (!empty($user->getPlainPassword())) {
                $encoder = $this->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($encoded);
            }

            $entityManager->merge($user);
            $entityManager->flush();

            return $user;
        }

        return $form;
    }

    protected function _userNotFound()
    {
        throw new NotFoundHttpException("User not found!");
    }

}
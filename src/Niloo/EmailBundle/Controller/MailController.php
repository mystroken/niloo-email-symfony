<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 10/02/2017
 * Time: 19:37
 */

namespace Niloo\EmailBundle\Controller;

use FOS\RestBundle\View\View;
use Niloo\EmailBundle\Entity\Mail;
use \Symfony\Component\Form\Form;
use Niloo\EmailBundle\Form\Type\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class MailController
 *
 * @package Niloo\EmailBundle\Controller
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class MailController extends Controller
{
    /**
     * Retrieve all mails of the database
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"mail"})
     * @Rest\Get("/mails")
     * @return array
     */
    public function getMailsAction() {
        $mails = $this->getDoctrine()->getRepository(Mail::class)->findBy([], ['id'=>'DESC']);
        return $mails;
    }


    /**
     * Retrieve a specific mail from the database
     *
     * @Rest\View(serializerGroups={"mail"})
     * @Rest\Get("/mails/{id}")
     * @param Request $request
     * @return object|JsonResponse
     */
    public function getMailAction(Request $request) {

        $mail = $this->getDoctrine()->getRepository(Mail::class)->find((int)$request->get('id'));
        if(empty($mail)) $this->_mailNotFound();
        return $mail;

    }

    /**
     * Add new mail
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"mail"})
     * @Rest\Post("/mails")
     * @param Request $request
     * @return object
     */
    public function postMailsAction(Request $request) {

        $mail = new Mail();

        $form = $this->createForm(MailType::class, $mail);
        $form->submit($request->request->all());

        if($form->isValid()) {
            $connectedUser = $this->get('security.token_storage')->getToken()->getUser();
            $mail->setSlug($this->get('app.slugger')->slugify($mail->getObject()));
            $mail->setCreatedBy($connectedUser);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mail);
            $entityManager->flush();

            return $mail;
        }

        // Return errors
        return $form;
    }


    /**
     * Delete a resource
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT, serializerGroups={"mail"})
     * @Rest\Delete("/mails/{id}")
     * @param Request $request
     */
    public function removeMailAction(Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $mail = $entityManager->getRepository(Mail::class)->find((int) $request->get('id'));

        if($mail) {

            foreach ($mail->getTopics() as $topic) {
                $entityManager->remove($topic);
            }

            $entityManager->remove($mail);
            $entityManager->flush();
        }
    }

    /**
     * Update (completely) a resource
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"mail"})
     * @Rest\Put("/mails/{id}")
     * @param Request $request
     * @return object|Form|JsonResponse
     */
    public function updateMailAction(Request $request) {
        return $this->_updateMail($request, true);
    }

    /**
     * Update partially the property of a resource
     *
     * @Rest\View(serializerGroups={"mail"})
     * @Rest\Patch("/mails/{id}")
     * @param Request $request
     * @return object|Form|JsonResponse
     */
    public function patchMailAction(Request $request) {
        return $this->_updateMail($request, false);
    }


    /**
     * Update partially or completely
     *
     * clearMissing is a boolean that tell to Symfony
     * to change or not the old value of a missing property in the request
     *
     * @param Request $request
     * @param bool $clearMissing
     * @return object|Form|JsonResponse
     */
    private function _updateMail(Request $request, $clearMissing = true) {
        $entityManager = $this->getDoctrine()->getManager();
        $mail = $entityManager->getRepository(Mail::class)->find((int)$request->get('id'));

        if(empty($mail)) {
            return View::create(['message' => 'The resource does not exist'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(MailType::class, $mail);
        $form->submit($request->request->all(), $clearMissing);

        if($form->isValid()) {
            $connectedUser = $this->get('security.token_storage')->getToken()->getUser();
            $mail->setSlug($this->get('app.slugger')->slugify($mail->getObject()));
            $mail->setUpdatedBy($connectedUser);
            $mail->setUpdatedAt(new \DateTime('now'));

            $entityManager->merge($mail);
            $entityManager->flush();
            return $mail;
        }

        return $form;
    }

    private function _mailNotFound() {
        throw new NotFoundHttpException("Mail not found!");
    }
}
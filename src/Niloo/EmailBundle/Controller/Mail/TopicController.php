<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 10/02/2017
 * Time: 19:37
 */

namespace Niloo\EmailBundle\Controller\Mail;

use FOS\RestBundle\View\View;
use Niloo\EmailBundle\Entity\Mail;
use Niloo\EmailBundle\Entity\Topic;
use Niloo\EmailBundle\Form\Type\TopicType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class TopicController
 *
 * @package Niloo\EmailBundle\Controller
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class TopicController extends Controller
{

    /**
     * Retrieve all topics of a mail
     *
     * @Rest\View(serializerGroups={"topic"})
     * @Rest\Get("/mails/{id}/topics")
     * @param Request $request
     * @return View|Response
     */
    public function getTopicsAction(Request $request) {
        $mail = $this->getDoctrine()->getRepository(Mail::class)->find((int)$request->get('id'));

        if(empty($mail)) {
            return $this->_postNotFound();
        }

        return $mail->getTopics();

    }

    /**
     * add new topic to a mail
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"topic"})
     * @Rest\Post("/mails/{id}/topics")
     * @param Request $request
     * @return object|JsonResponse
     */
    public function getTopicAction(Request $request) {
        $manager = $this->getDoctrine()->getManager();
        $mail = $manager->getRepository(Mail::class)->find((int)$request->get('id'));

        if(empty($mail)) {
            return $this->_postNotFound();
        }

        $topic = new Topic();
        $topic->setMail($mail);

        $form = $this->createForm(TopicType::class, $topic);
        $form->submit($request->request->all());

        if($form->isValid()) {
            $manager->persist($topic);
            $manager->flush();
            return $topic;
        }

        return $form;
    }

    /**
     * @return View
     */
    private function _postNotFound() {
        throw new NotFoundHttpException("Mail not found!");
    }
}
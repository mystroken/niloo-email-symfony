<?php
/**
 * Created by IntelliJ IDEA.
 * User: ken
 * Date: 12/02/2017
 * Time: 18:26
 */

namespace Niloo\EmailBundle\Controller;


use Niloo\EmailBundle\Entity\Credentials;
use Niloo\EmailBundle\Entity\Token;
use Niloo\EmailBundle\Entity\User;
use Niloo\EmailBundle\Form\Type\CredentialsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Form;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class TokenController
 * @package Niloo\EmailBundle\Controller
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class TokenController extends Controller
{
    /**
     * Ask a token
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"token"})
     * @Rest\Post("/tokens")
     * @param Request $request
     * @return Token|\Symfony\Component\Form\Form|static
     */
    public function postTokensAction(Request $request)
    {
        $credentials = new Credentials();
        $form = $this->createForm(CredentialsType::class, $credentials);

        $form->submit($request->request->all());

        if(!$form->isValid()) {
            return $form;
        }

        // Now we look if the credentials are for a user
        $em = $this->getDoctrine()->getManager();
        $user= $em->getRepository(User::class)->findOneByEmail($credentials->getLogin());

        // if the user not found
        if(!$user) {
            return $this->_invalidCredentials();
        }

        // We check if password match with the user
        $encoder = $this->get('security.password_encoder');
        $isPasswordValid = $encoder->isPasswordValid($user, $credentials->getPassword());

        // If the password is not correct
        if((!$isPasswordValid)) {
            return $this->_invalidCredentials();
        }

        // Else, we can create a token for the user
        $token = new Token();
        $token->setValue(base64_encode(random_bytes(50)));
        $token->setUser($user);

        $em->persist($token);
        $em->flush();

        return $token;
    }


    /**
     * Log out a token
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/tokens/{id}")
     * @param Request $request
     */
    public function removeTokenAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $token = $em->getRepository(Token::class)->find((int) $request->get('id'));

        $connectedUser = $this->get('security.token_storage')->getToken()->getUser();

        if ($token && $token->getUser()->getId() === $connectedUser->getId()) {
            $em->remove($token);
            $em->flush();
        }

        throw new BadRequestHttpException();
    }


    private function _invalidCredentials()
    {
        throw new BadRequestHttpException("Invalid credentials");
    }
}
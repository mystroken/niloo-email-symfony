<?php
namespace Niloo\EmailBundle\DtaFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Niloo\EmailBundle\Entity\Mail;
use Niloo\EmailBundle\Entity\Topic;
use Niloo\EmailBundle\Entity\User;

/**
 * Created by PhpStorm.
 * User: ken
 * Date: 10/02/2017
 * Time: 21:46
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class LoadMailData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // Create a user
        $user = new User();

        $firstMail = new Mail();
        $firstMail->setObject("Test of a mail");
        $firstMail->setSlug("test-of-a-mail");

        $firstTopic = new Topic();
        $firstTopic->setTitle("First topic");

        $secondTopic = new Topic();
        $secondTopic->setTitle("Second topic");

        $thirdTopic = new Topic();
        $thirdTopic->setTitle("Third title");

        $firstMail->addTopic($firstTopic);
        $firstMail->addTopic($secondTopic);
        $firstMail->addTopic($thirdTopic);

        $manager->persist($firstMail);
        $manager->flush();
    }
}
<?php

namespace Niloo\EmailBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Niloo\EmailBundle\Entity\Topic;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Mail
 *
 * @Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity({"object"})
 * @ORM\Table(name="mails", uniqueConstraints={@ORM\UniqueConstraint(name="users_email_unique",columns={"object"})})
 * @ORM\Entity(repositoryClass="Niloo\EmailBundle\Repository\MailRepository")
 */
class Mail
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="object", type="string", length=255, unique=true)
     */
    private $object;

    /**
     * @var string
     *
     * @ORM\Column(name="subobject", type="string", nullable=true)
     */
    private $subobject;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $updatedBy;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Topic", mappedBy="mail", cascade={"persist"})
     */
    private $topics;




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->topics = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set object
     *
     * @param string $object
     *
     * @return Mail
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Mail
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Mail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Mail
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add topic
     *
     * @param Topic $topic
     *
     * @return Mail
     */
    public function addTopic(Topic $topic)
    {
        $topic->setMail($this);
        $this->topics[] = $topic;

        return $this;
    }

    /**
     * Remove topic
     *
     * @param Topic $topic
     */
    public function removeTopic(Topic $topic)
    {
        $topic->setMail(null);
        $this->topics->removeElement($topic);
    }

    /**
     * Get topics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Set subobject
     *
     * @param string $subobject
     *
     * @return Mail
     */
    public function setSubobject($subobject)
    {
        $this->subobject = $subobject;

        return $this;
    }

    /**
     * Get subobject
     *
     * @return string
     */
    public function getSubobject()
    {
        return $this->subobject;
    }

    /**
     * Set createdBy
     *
     * @param \Niloo\EmailBundle\Entity\User $createdBy
     *
     * @return Mail
     */
    public function setCreatedBy(\Niloo\EmailBundle\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Niloo\EmailBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \Niloo\EmailBundle\Entity\User $updatedBy
     *
     * @return Mail
     */
    public function setUpdatedBy(\Niloo\EmailBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Niloo\EmailBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}

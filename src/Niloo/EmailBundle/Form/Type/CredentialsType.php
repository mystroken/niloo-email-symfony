<?php
/**
 * Created by IntelliJ IDEA.
 * User: ken
 * Date: 12/02/2017
 * Time: 18:15
 */

namespace Niloo\EmailBundle\Form\Type;


use Niloo\EmailBundle\Entity\Credentials;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CredentialsType
 * @package Niloo\EmailBundle\Form\Type
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class CredentialsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('login');
        $builder->add('password');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Credentials::class,
            'csrf_protection' => false
        ]);
    }
}
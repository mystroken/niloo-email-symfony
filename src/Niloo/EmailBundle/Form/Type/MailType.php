<?php
namespace Niloo\EmailBundle\Form\Type;


use Niloo\EmailBundle\Entity\Mail;
use Niloo\EmailBundle\Entity\Topic;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MailType
 * @package Niloo\EmailBundle\Form\type
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class MailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object')
            ->add('subobject')
            ->add('topics', CollectionType::class, [
                "entry_type" => TopicType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "delete_empty" => true,
                "by_reference" => false
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mail::class,
            'csrf_protection' => false
        ]);
    }
}
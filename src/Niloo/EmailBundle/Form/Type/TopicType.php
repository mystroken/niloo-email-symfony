<?php
/**
 * Created by PhpStorm.
 * User: ken
 * Date: 11/02/2017
 * Time: 11:28
 */

namespace Niloo\EmailBundle\Form\Type;

use Niloo\EmailBundle\Entity\Mail;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TopicType
 * @package Niloo\EmailBundle\Form\type
 * @author Emmanuel KWENE <njume48@gmail.com>
 */
class TopicType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content', TextareaType::class);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Niloo\EmailBundle\Entity\Topic',
            'csrf_protection' => false
        ]);
    }
}
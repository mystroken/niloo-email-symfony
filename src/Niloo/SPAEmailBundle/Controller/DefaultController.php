<?php

namespace Niloo\SPAEmailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('NilooSPAEmailBundle:Default:index.html.twig');
    }
}
